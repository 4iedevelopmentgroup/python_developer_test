import logging


"""
Log levels available to application
"""
LOG_LEVELS = {
        logging.getLevelName(logging.CRITICAL): logging.CRITICAL,
        logging.getLevelName(logging.ERROR): logging.ERROR,
        logging.getLevelName(logging.WARNING): logging.WARNING,
        logging.getLevelName(logging.INFO): logging.INFO,
        logging.getLevelName(logging.DEBUG): logging.DEBUG
    }


"""
Initialize global logger
"""
logger = logging.getLogger('')
logger.setLevel(logging.WARNING)
logging.basicConfig(format='[%(asctime)s][%(levelname)s] %(message)s')


def set_log_level(level):
    """
    Function to overwrite logging level
    """
    global logger
    logger.setLevel(validate_log_level(level))


def validate_log_level(level):
    """
    Function to validate log level
    """
    level = level.upper()
    if level not in LOG_LEVELS:
        logging.error('Invalid log level specified')
        exit(-1)
    return LOG_LEVELS[level]
