from csvreader.csvreader import CSVReader
from logger.logger import logger
from difflib import SequenceMatcher
import time
import json


def display_first_five_sorted_records_asc(path, verbose=True):
    """
    Answer to question 1 of the Python developer test
    Prints the first 5 lines of Current Rent after ordered ascending
    """
    try:
        # create reader
        reader = CSVReader()

        # read data from csv
        read_data = reader.read_csv_data(path)

        # sort data
        sorted_data = reader.order_csv_data(
            'Current Rent',
            read_data,
            CSVReader.ASCENDING
        )[:5]

        # display data as json
        if verbose:
            print(json.dumps(sorted_data))
        else:
            return sorted_data

    except FileNotFoundError:
        logger.error('File not found. Please provide a valid path')
    except ValueError as e:
        logger.error(e)
    except Exception as e:
        logger.error(e)


def print_calculated_lease_years(path, lease_years=25, verbose=True):
    """
    Answer to question 2 of the Python developer test
    Prints and calculates the total of all records with a 25 Lease Year
    """
    try:
        # create reader
        reader = CSVReader()

        # read data from csv
        read_data = reader.read_csv_data(path, [{
            'column': 'Lease Years',
            'operator': '=',
            'value': lease_years
        }])

        # calculate total
        console_out = {'data': [], 'total': 0}
        for data in read_data:
            try:
                console_out['total'] += float(data['Current Rent'])
                console_out['data'].append(data)
            except ValueError:
                logger.debug('Skipping invalid entry.')

        # display data as json
        if verbose:
            print(json.dumps(console_out))
        else:
            return console_out

    except FileNotFoundError:
        logger.error('File not found. Please provide a valid path')
    except ValueError as e:
        logger.error(e)
    except Exception as e:
        logger.error(e)


def print_tenant_totals(path, tolerance=0.5, verbose=True):
    """
    Answer to question 3 of the Python developer test
    Function to retrieve tenant totals
    """
    try:
        # create reader
        reader = CSVReader()

        # read data from csv
        read_data = reader.read_csv_data(path)

        # init
        tenant_names = []
        tenant_totals = {}

        # loop csv data
        for data in read_data:
            # check if name is exact match
            if data['Tenant Name'] in tenant_totals:
                tenant_names.append(data['Tenant Name'])
                tenant_totals[data['Tenant Name']] += 1
                continue

            # check if name is close match
            found_match = False
            for tn in tenant_names:
                if SequenceMatcher(a=data['Tenant Name'], b=tn).ratio() >\
                        tolerance:
                    tenant_totals[tn] += 1
                    found_match = True
                    break

            # create if not found
            if not found_match:
                tenant_names.append(data['Tenant Name'])
                tenant_totals[data['Tenant Name']] = 1

        # display tenant totals
        console_out = []
        for tenant_name in tenant_totals:
            if verbose:
                print('Tenant %s has %s masts.'
                      % (tenant_name, tenant_totals[tenant_name]))
            else:
                console_out.append({tenant_name: tenant_totals[tenant_name]})

        # check return
        if not verbose:
            return console_out
    except FileNotFoundError:
        logger.error('File not found. Please provide a valid path')
    except ValueError as e:
        logger.error(e)
    except Exception as e:
        logger.error(e)


def print_tenants_between(path, date_from='1 Jun 1999', date_to='31 Aug 2007',
                          input_format='%d %b %Y', output_format='%d/%m/%Y',
                          verbose=True):
    """
    Answer for question 4 of the Python developer test
    Function to print tenants with lease start dates between 2 specific dates
    """
    try:
        # times to epoch
        start = to_epoch(date_from, input_format)
        end = to_epoch(date_to, input_format)

        # create reader
        reader = CSVReader()

        # read data from csv
        read_data = reader.read_csv_data(path)

        # loop csv data
        output_data = []
        for data in read_data:
            # check dates
            dts = to_epoch(data['Lease Start Date'], input_format)
            dte = to_epoch(data['Lease End Date'], input_format)
            if start < dts < end:
                # update date format
                data['Lease Start Date'] = time_to_str(dts, output_format)
                data['Lease End Date'] = time_to_str(dte, output_format)

                # add to output
                output_data.append(data)

        # display
        if verbose:
            print(json.dumps(output_data))
        else:
            return output_data

    except FileNotFoundError:
        logger.error('File not found. Please provide a valid path')
    except ValueError as e:
        logger.error(e)
    except Exception as e:
        logger.error(e)


def to_epoch(date, format='%d %b %Y'):
    """
    Function to changes date string into epoch time
    """
    return time.mktime(time.strptime(date, format))


def time_to_str(t, format='%d/%m/%Y'):
    """
    Function to change epoch time into date string
    """
    return time.strftime(format, time.localtime(t))
