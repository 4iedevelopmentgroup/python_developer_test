import unittest
import os
from csvpropertyreader.csvpropertyreader import \
    display_first_five_sorted_records_asc, \
    print_calculated_lease_years, print_tenant_totals, \
    print_tenants_between, \
    to_epoch


class TestCSVPropertyReader(unittest.TestCase):

    """
    Path to default data set
    """
    TEST_DATASET_PATH = "%s/../res/csv/default_dataset.csv" \
                        % os.path.dirname(os.path.realpath(__file__))

    def test_question_1(self):
        """
        Test for question 1 of the Python developer test
        Test done on default_dataset.csv
        """
        data = display_first_five_sorted_records_asc(
            TestCSVPropertyReader.TEST_DATASET_PATH,
            False
        )
        if not data:
            raise RuntimeError("Failed to get data from data set")

        # check length (must be 5)
        self.assertEqual(len(data), 5)

        # test if data is sorted
        first_run = True
        prev_current_rent = 0
        for d in data:
            # check if is sorted
            if not first_run:
                self.assertLessEqual(
                    float(prev_current_rent),
                    float(d["Current Rent"])
                )

            # set previous rent
            first_run = False
            prev_current_rent = d["Current Rent"]

    def test_question_2(self):
        """
        Test for question 2 of the Python developer test
        Test done on default_dataset.csv
        """
        data = print_calculated_lease_years(
            TestCSVPropertyReader.TEST_DATASET_PATH,
            verbose=False
        )
        if not data:
            raise RuntimeError("Failed to get data from data set")

        # check lease year
        for d in data["data"]:
            self.assertEqual(float(d["Lease Years"]), 25)

        # check total
        self.assertEqual(data["total"], 46500.0)

    def test_question_3(self):
        """
        Test for question 3 of the Python developer test
        Test done on default_dataset.csv
        """
        data = print_tenant_totals(
            TestCSVPropertyReader.TEST_DATASET_PATH,
            verbose=False
        )
        if not data:
            raise RuntimeError("Failed to get data from data set")

        # return on this data set should equal the following
        correct_value = [
            {'Arqiva Services ltd': 2},
            {'Vodafone Ltd.': 2},
            {'O2 (UK) Ltd': 1},
            {'Hutchinson3G Uk Ltd&Everything Everywhere Ltd': 21},
            {'Cornerstone Telecommunications Infrastructure': 16}
        ]

        # test for correct value
        self.assertEqual(data, correct_value)

    def test_question_4(self):
        """
        Test for question 4 of the Python developer test
        Test done on default_dataset.csv
        """
        data = print_tenants_between(
            TestCSVPropertyReader.TEST_DATASET_PATH,
            verbose=False
        )
        if not data:
            raise RuntimeError("Failed to get data from data set")

        # test dates
        start = to_epoch('1 Jun 1999')
        end = to_epoch('31 Aug 2007')

        # check dates
        for d in data:
            try:
                # check date format and convert to epoch
                # exception will occur for invalid date formats
                lease_start_date = to_epoch(d["Lease Start Date"], "%d/%m/%Y")
                to_epoch(d["Lease End Date"], "%d/%m/%Y")

                # check if start date is between test dates
                self.assertGreater(lease_start_date, start)
                self.assertLess(lease_start_date, end)
            except ValueError:
                raise RuntimeError("Some records contain invalid date format")


if __name__ == '__main__':
    unittest.main()
