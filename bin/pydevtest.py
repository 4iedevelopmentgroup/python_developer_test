import argparse
from logger.logger import set_log_level
import os
from csvpropertyreader.csvpropertyreader import \
    display_first_five_sorted_records_asc, print_calculated_lease_years, \
    print_tenant_totals, \
    print_tenants_between


if __name__ == "__main__":
    # args parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-p',
        '--path',
        help='The path to the csv file that needs to be processed',
        default='%s/res/csv/default_dataset.csv' %
                os.path.dirname(os.path.realpath(__file__))
    )
    parser.add_argument(
        '-l',
        '--log',
        help="Sets the logging level",
        default='warning'
    )
    parser.add_argument(
        '-q',
        '--question',
        help='The question number on the Python developer test',
        default=1,
        choices=['1', '2', '3', '4']
    )
    args = parser.parse_args()

    # set logging level
    set_log_level(args.log)

    # run selected question
    if args.question == '1':
        display_first_five_sorted_records_asc(args.path)
    elif args.question == '2':
        print_calculated_lease_years(args.path)
    elif args.question == '3':
        print_tenant_totals(args.path)
    else:
        print_tenants_between(args.path)
