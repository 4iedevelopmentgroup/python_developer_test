import os
import csv
from logger.logger import logger


class CSVReader:
    ASCENDING = 1
    DESCENDING = 2
    OPERATORS = ['=', '>', '<', '>=', '<=', 'in']

    def _validate_csv_file(self, path):
        """
        Function to validate csv file
        Checks exists and extension
        """
        if not os.path.exists(path):
            raise FileNotFoundError

        if path[-4:] != '.csv':
            raise ValueError('The specified file must be a .csv file')

    def _read(self, path, queries=None):
        """
        Function to read a csv file into a dictionary
        Queries are handled as an AND operator
        """
        try:
            data = []
            with open(path) as csv_file:
                reader = csv.DictReader(csv_file, delimiter=',')
                for row in reader:
                    if self._can_add_by_queries(row, queries):
                        data.append(row)
            return data
        except Exception as e:
            logger.error('Failed to load csv file into buffer. %s' % e)
            return []

    def _can_add_by_queries(self, row, queries=None):
        """
        Function to validate queries if there are any
        Queries are handled as an AND operator
        """
        # check if there is a query
        if not queries:
            return True

        # loop queries
        for query in queries:
            # validate query
            if 'column' not in query \
                    or 'operator' not in query \
                    or 'value' not in query:
                logger.error('Invalid query passed. All queries require '
                             'column, operator and value fields')
                return False

            # check if col exists
            if query["column"] not in row:
                logger.error('Data set does not contain column [ %s ]'
                             % query['column'])
                return False

            # check operators
            if query['operator'].lower() not in self.OPERATORS:
                logger.error('Operator [ %s ] can not be used'
                             % query['operator'])
                return False

            # cast values
            val = self._cast_value(query['value'])
            col = self._cast_value(row[query['column']])

            # operator [ = ]
            if query['operator'] == '=' and col != val:
                return False

            # operator [ > ]
            if query['operator'] == '>' and col <= val:
                return False

            # operator [ < ]
            if query['operator'] == '<' and col >= val:
                return False

            # operator [ <= ]
            if query['operator'] == '<=' and col > val:
                return False

            # operator [ >= ]
            if query['operator'] == '>=' and col < val:
                return False

            # operator [ in ]
            if query['operator'] == 'in' and col not in val:
                return False

        # success
        return True

    def read_csv_data(self, path, queries=None):
        """
        Function to validate and read csv file into a dictionary
        Queries are handled as an AND operator
        """
        self._validate_csv_file(path)
        return self._read(path, queries)

    def order_csv_data(self, col, data, asc_desc=ASCENDING):
        """
        Function to sort the values of a specific column in a given data set
        """
        cdata = data
        for i in range(len(cdata)-1, 0, -1):
            for idx in range(i):
                # asc or desc
                values = self._asc_desc_val(
                    self._cast_value(cdata[idx][col]),
                    self._cast_value(cdata[idx + 1][col]),
                    asc_desc
                )

                # check if value should be removed
                if values[0] > values[1]:
                    temp = cdata[idx]
                    cdata[idx] = cdata[idx + 1]
                    cdata[idx + 1] = temp
        return cdata

    def _asc_desc_val(self, val1, val2, asc_desc):
        """
        Function to use value based on direction
        """
        if asc_desc == self.ASCENDING:
            return [val1, val2]
        else:
            return [val2, val1]

    def _cast_value(self, value):
        """
        Function to cast value
        String comparisons are different from number comparisons
        """
        try:
            return float(value)
        except ValueError:
            return value
