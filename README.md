# Python Developer Test
Built in Python 3

## How to install:

### Clone the repository
git clone https://4iedevelopmentgroup@bitbucket.org/4iedevelopmentgroup/python_developer_test.git

### Install the environment (--dev optional - only if you want to run flake8)
pipenv install --dev

## How to run:

#### Activate Virtual Environment
pipenv shell

#### Run Question 1
python ./bin/pydevtest.py -q 1

#### Run Question 2
python ./bin/pydevtest.py -q 2

#### Run Question 3
python ./bin/pydevtest.py -q 3

#### Run Question 4
python ./bin/pydevtest.py -q 4

#### Output:
Questions 1, 2 and 4 are outputted in JSON format.  
Question 3's output is in a readable form. 

#### Optional Parameters Are:
-p : The path to the csv file that needs to be processed  
-l : Sets the logging level  
-q : The question number on the Python developer test  

## How to run unit tests:

#### Enter bin directory
cd ./bin/

#### Run the following cmd
python -m unittest

## How to run flake8 on code:

#### Activate Virtual Environment
pipenv shell

#### Run flake8 on the bin directory
flake8 ./bin/